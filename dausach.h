#pragma once
#include <iostream>
#include <string.h>
#include <windows.h>
#include "menu.h"
#include<fstream>
#include "nhapdg.h"
#include "sach.h"
#include <string>
using namespace std;


const int MAX = 4000;

struct DauSach
{
	int MaDS;
	char TenSach[100];
	int SoTrang;
	char TacGia[50];
	int NamXuatBan;
	char TheLoai[40];
	PTRS ConTroS = NULL;
};

struct List{
	int n;
	DauSach DS[MAX];
};


void ghis(Sach s, ofstream &m){
	m<<s.MaSach; m<<",";
	m<<s.TrangThai; m<<",";
	m<<s.ViTri; m<<",";
}
void GhiFile(DauSach DS[], int n)
{ 	int dem; 
	ofstream fw("DauSach.txt", ios::out ); 
	fw<<n; fw<<endl;
	for (int i = 0; i<n; i++){
		dem=0;
		for(PTRS a = DS[i].ConTroS; a!=NULL; a=a->next) dem++;
		fw<<dem; fw<<",";
		for(PTRS a = DS[i].ConTroS; a!=NULL; a=a->next) ghis(a->S,fw);
		fw<<DS[i].MaDS; fw<<",";
		fw<<DS[i].TenSach; fw<<",";
		fw<<DS[i].TacGia; fw<<",";
		fw<<DS[i].SoTrang; fw<<",";
		fw<<DS[i].NamXuatBan; fw<<",";
		fw<<DS[i].TheLoai; fw<<endl;
	}		
	fw.close(); 
}

void DocFile(DauSach Ds[], int &n) 
{
	string g; Sach s;
	
	int m=1;
	ifstream doc("DauSach.txt", ios::in);
	getline(doc,g); n=atoi(g.c_str());
	while(m<=n){
		string sosach, mads, tensach, tacgia,sotrang, namxb, theloai;
		getline(doc,sosach,','); int dem = atoi(sosach.c_str());
		while(dem>0){
			string masach, trangthai,vitri;
			getline(doc,masach,','); s.MaSach = atoi(masach.c_str());
			getline(doc,trangthai,','); s.TrangThai = atoi(trangthai.c_str());
			getline(doc,vitri,','); for(int j=0;j<vitri.length();j++) s.ViTri[j]=vitri[j];
			s.ViTri[vitri.length()]='\0'; 
			InsertOrderS(Ds[m-1].ConTroS,s);
			dem--;
		}
		getline(doc,mads,','); Ds[m-1].MaDS = atoi(mads.c_str());
		getline(doc,tensach,','); for(int j=0;j<tensach.length();j++) Ds[m-1].TenSach[j]=tensach[j];
			Ds[m-1].TenSach[tensach.length()]='\0'; 
		getline(doc,tacgia,','); for(int j=0;j<tacgia.length();j++) Ds[m-1].TacGia[j]=tacgia[j];
			Ds[m-1].TacGia[tacgia.length()]='\0'; 
		getline(doc,sotrang,','); Ds[m-1].SoTrang = atoi(sotrang.c_str());
		getline(doc,namxb,','); Ds[m-1].NamXuatBan = atoi(namxb.c_str());
		getline(doc,theloai); for(int j=0;j<theloai.length();j++) Ds[m-1].TheLoai[j]=theloai[j];
			Ds[m-1].TheLoai[theloai.length()]='\0'; 
		m++;
	}

	doc.close();
}

void XoaXuat();

void Xuat(DauSach D, int i, int j)
{
	gotoxy(2,5+i); cout<<j;
	gotoxy(6,5+i); cout<< D.MaDS;
	gotoxy(14,5+i); XuatChuoi(D.TenSach,32,15,5,i);
	gotoxy(48,5+i); cout	<<D.SoTrang;
	gotoxy(57,5+i); XuatChuoi(D.TacGia,25,58,5,i);
	gotoxy(83,5+i); cout	<<D.NamXuatBan;
	gotoxy(90,5+i); XuatChuoi(D.TheLoai,16,91,5,i);
	
}
void KhungDS();
int XuatDS(DauSach DS[], int n)
{	
	int z = 0; int j= -1,i=1;
	XoaXuat();
	ThongBao("ESC: Ve MENU",3,140,38,17,3);
	ThongBao("UP: Truoc",4,118,42,17,3);
	ThongBao("DOWN: Sau",4,140,42,17,3);
	for(i=1; i<=n; i++) {	
		j++; 
		Xuat(DS[i-1], j*3+1, i);
		if(i%16==0 && i<n)	break;
	}
		while(z!=82 && z!=27){			
						
			z = getch();
			if(z==72){
				textcolor(12); ThongBao("UP: Truoc",4,118,42,17,3); Sleep(300); textcolor(0);	ThongBao("UP: Truoc",4,118,42,17,3);
			}
			if(z==80){				
				textcolor(12);	ThongBao("DOWN: Sau",4,140,42,17,3); Sleep(300); textcolor(0); ThongBao("DOWN: Sau",4,140,42,17,3); 
			}
			if(z==72&& i>16 ){
				j=-1; i--; i=(i/16 - 1)*16 +1;
				XoaXuat();
				for(i; i<=n; i++) {	
					j++; 
					Xuat(DS[i-1], j*3+1, i);
					if(i!=0 && i%16==0)	break;	
				}
			}
			else if(z==80 && i%16==0){
				j=-1; 
				XoaXuat();
				for(i=i+1; i<=n; i++) {	
					j++;
					Xuat(DS[i-1], j*3+1, i);	
					if(i!=0 && i%16==0 ) break;
				}
			}
			else if(z==82){
				XoaXuat();
				j=-1; 
				for(i=(n/16)*16+1; i<=n; i++) {	
					j++;
					Xuat(DS[i-1], j*3+1, i);		
				} return 82;
			}
			else if(z==27) {
				textcolor(12); ThongBao("ESC: Ve MENU",3,140,38,17,3); Sleep(300); textcolor(0); ThongBao("ESC: Ve MENU",3,140,38,17,3); 
				j=-1; 
				XoaXuat();
				for(i=(n/16)*16+1; i<=n; i++) {	
					j++;
					Xuat(DS[i-1], j*3+1, i);  	
				} return 27;
			}
		}
	
}


int Nhap1DauSach(DauSach DS[], int &n)
{
	Yescursortype();
	n++; if(n>=MAX){n--; return 0;} int kt=0;	
	gotoxy(126,17); do{
		kt=0;
		gotoxy(126,17); for(int j=0;j<7;j++) cout<<" ";
	ThongBao("MA DAU SACH co 5 chu so",1,125,10,24,3);
		if(NhapMa(DS[n-1].MaDS,126,17,120,5,10000, 99999)==27){n--; return 0;} 
		XoaTB(125,10,24,3);
		for(int i=0; i<n-1;i++) if(DS[i].MaDS==DS[n-1].MaDS){
			ThongBao("Ma DAU SACH bi trung!",1,125,5,22,3); Sleep(500); XoaTB(125,5,22,3); kt=1; break;
		}
	} while(kt==1);
	gotoxy(126,20); fflush(stdin); if(NhapChuoi(DS[n-1].TenSach,126,20,117,5,30)==27) {n--; return 0;} 
	ChuanHoa(DS[n-1].TenSach, strlen(DS[n-1].TenSach));
	gotoxy(126,23); fflush(stdin); if(NhapMa(DS[n-1].SoTrang,126,23,117,5,1,99999)==27) {n--; return 0;}
	gotoxy(126,26);	fflush(stdin); if(NhapChuoi(DS[n-1].TacGia, 126,26,117,5,23)==27) {n--; return 0;}
	ChuanHoa(DS[n-1].TacGia, strlen(DS[n-1].TacGia));
	gotoxy(126,29); if(NhapMa(DS[n-1].NamXuatBan, 126,29,117,5,1,9999)==27) {n--; return 0;}
	gotoxy(126,32);	fflush(stdin); if(NhapChuoi(DS[n-1].TheLoai,126,32, 117,5,14)==27){n--; return 0;}
	ChuanHoa(DS[n-1].TheLoai, strlen(DS[n-1].TheLoai));	
	Nocursortype();
}

void XoaNhap(){
	textcolor(7); //cung mau BG
	for(int i=0; i<=17;i++){
		gotoxy(126,17+i);
		for(int j=0;j<39;j++) cout<<char(219);
	}
	textcolor(0);
}


void KhungNhapDS(){
	VeKhung(110,15,20,55);
	gotoxy(112,17); cout<<"Ma Dau Sach: ";
	gotoxy(112,20); cout<<"Ten Sach: ";
	gotoxy(112,23); cout<<"So Trang: ";
	gotoxy(112,26); cout<<"Tac Gia: "; 
	gotoxy(112,29); cout<<"Nam Xuat Ban: ";
	gotoxy(112,32); cout<<"The Loai: ";
}

void KhungDS(){	
	gotoxy(42,1); cout<<"BANG THONG TIN DAU SACH";
	int a1=1, b4 = 3, Cot = 50, Hang = 105;
	VeKhung(a1,b4,Cot,Hang);
	
	VeDoc(a1+4,b4,Cot-1);
	VeNgang(a1+1,b4+2,Hang-1);	
	gotoxy(a1,b4+2); cout<<char(GiuaTrai);
	gotoxy(a1+4,b4); cout<<char(GiuaTren);
	gotoxy(a1+4,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+4,Cot+3); cout<<char(GiuaDuoi);
	gotoxy(a1+Hang,b4+2); cout<<char(GiuaPhai);
	gotoxy(a1+1,b4+1); cout<<"STT";
	
	gotoxy(a1+6,b4+1); cout<<"MA DS";
	VeDoc(a1+12,b4,Cot-1);
	gotoxy(a1+12,b4); cout<<char(GiuaTren);
	gotoxy(a1+12,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+12,Cot+3); cout<<char(GiuaDuoi);
	
	gotoxy(a1+24,b4+1); cout<<"TEN SACH";
	VeDoc(a1+46,b4,Cot-1);
	gotoxy(a1+46,b4); cout<<char(GiuaTren);
	gotoxy(a1+46,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+46,Cot+3); cout<<char(GiuaDuoi);
	
	gotoxy(a1+47,b4+1); cout<<"SO TRANG";
	VeDoc(a1+55,b4,Cot-1);
	gotoxy(a1+55,b4); cout<<char(GiuaTren);
	gotoxy(a1+55,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+55,Cot+3); cout<<char(GiuaDuoi);
	
	gotoxy(a1+62,b4+1); cout<<"TEN TAC GIA";
	
	gotoxy(a1+82,b4+1); cout<<"NAM XB";
	VeDoc(a1+81,b4,Cot-1);
	gotoxy(a1+81,b4); cout<<char(GiuaTren);
	gotoxy(a1+81,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+81,Cot+3); cout<<char(GiuaDuoi);
	
	gotoxy(a1+92,b4+1); cout<<"THE LOAI";
	VeDoc(a1+88,b4,Cot-1);
	gotoxy(a1+88,b4); cout<<char(GiuaTren);
	gotoxy(a1+88,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+88,Cot+3); cout<<char(GiuaDuoi);
	for(int i=0;i<15;i++){
		gotoxy(1,8+i*3); for(int j=0;j<105; j++){
		if(j==0 || j==4 || j==12 || j==46 || j==55 || j==81 || j==88) gotoxy(1+j+1,8+i*3);
		else  cout<<char(196);
		}
	}
	
}

void XoaXuat(){
	textcolor(7);
	for(int i=0; i<47;i++){
		if((i+1)%3==0) ;
		else {
			gotoxy(2,6+i); for(int j=0;j<3;j++)	 cout<<char(219); 
		gotoxy(6,6+i); for(int j=0; j< 7 ;j++) cout<<char(219); 
		gotoxy(14,6+i); for(int j=0; j< 33 ;j++) cout<<char(219); 
		gotoxy(48,6+i); for(int j=0; j< 8 ;j++) cout<<char(219);
		gotoxy(57,6+i); for(int j=0; j< 25 ;j++) cout<<char(219); 
		gotoxy(83,6+i); for(int j=0; j< 6 ;j++) cout<<char(219); 
		gotoxy(90,6+i); for(int j=0; j< 16 ;j++) cout<<char(219); 
		}
	}
	textcolor(0);
}

void NhapDauSach(DauSach DS[], int &n){
	KhungDS();	
	int a=0,b=1;
	while(a!=27){
	
		KhungNhapDS();			
		ThongBao("INSERT: Nhap Moi",1, 118,38,17,3);
		a = XuatDS(DS,n);
		
		if(a==82){	
			textcolor(12); ThongBao("INSERT: Nhap Moi",1, 118,38,17,3); Sleep(300);
			XoaTB(118,38,17,3);  XoaTB(118,42,17,3); XoaTB(140,42,17,3);
			XoaNhap();		
			b= Nhap1DauSach(DS,n);
			XoaXuat();
		}
		if(a==27 || b==0){
			textcolor(12);
			ThongBao("ESC: Ve MENU",3,140,38,17,3); Sleep(300);
			 break;
		}
	}
}

int SapXepTheoTen(List &ds){
	int i,j; DauSach DSa;
	for(i=0; i<ds.n;i++)
		for(j=i;j<ds.n;j++)
			if(stricmp(ds.DS[i].TenSach,ds.DS[j].TenSach)>0){
				DSa = ds.DS[i];
				ds.DS[i] = ds.DS[j];
				ds.DS[j] = DSa;
			}
}

int TimKiem(List &ds, char *Ten){
	int i,j=0;
	for(i=0; i<ds.n; i++)
		if(stricmp(ds.DS[i].TenSach,Ten)==0){
			j=1;
			Xuat(ds.DS[i],i+1,1);
		}
	if(j==0) return 0;	
}

void Xoa1(int n, int vitri){
	//for
}

void Xuat1S(DauSach DS, Sach s,int &STTX, int &STT){
	STTX++; STT++;
	gotoxy(3,3+STTX*3); cout<<STT;
	gotoxy(9,3+STTX*3); cout<<DS.MaDS;
	gotoxy(20,3+STTX*3); XuatChuoi(DS.TenSach,35,21,6,(STTX-1)*3);
	gotoxy(56,3+STTX*3); cout<<s.MaSach;
	gotoxy(69,3+STTX*3); if(s.TrangThai==0)cout<<"Chua Muon"; if(s.TrangThai==1) cout<<"Da Muon"; if(s.TrangThai==2) cout<<"Thanh Ly";
	gotoxy(80,3+STTX*3); cout<<s.ViTri;	
}

int XuatSach(DauSach DS[], int n){
	KhungSach();
	int STT=0, STTX=0, z=0, i, dem=0, m=0, cuoi =0;
	for(i=0; i<n;i++){
		for(PTRS a=DS[i].ConTroS;a!=NULL;a=a->next){			
			if(STTX>=0 && STTX <17) {
				Xuat1S(DS[i],a->S,STTX,STT);
			}
			m++;
		} 
	}
	ThongBao("ESC: Ve MENU",1,132,40,13,3); ThongBao("UP: Trang Truoc",1,120,44,17,3); ThongBao("DOWN: Trang Sau",1,140,44,17,3);
	while(1){
		z=getch();
		if(z==72) 	NhayTB("UP: Trang Truoc",1,120,44,17,3);
		if(z==80)	NhayTB("DOWN: Trang Sau",1,140,44,17,3);
		if(z==72 && STT>17){
			XoaXuatS(); dem=-1;
			STTX=0; cuoi =STT%17; 
			if(cuoi==0) STT-=34;
			else STT = STT-(cuoi+17);
			cuoi=STT;
			for(i=0; i<n;i++){
				for(PTRS a=DS[i].ConTroS;a!=NULL;a=a->next){
					dem++;
					if(dem>=cuoi && dem<cuoi+17){
						Xuat1S(DS[i],a->S,STTX,STT);
					}
					if (dem>cuoi +17) break;
				} if (dem>cuoi +17) break;
			}
		}
		else if(z==80 && STT!=m){
			XoaXuatS(); dem=-1; 
			STTX=0; cuoi =STT;
			for(i=0; i<n;i++){
				for(PTRS a=DS[i].ConTroS;a!=NULL;a=a->next){
					dem++;
					if(dem>=cuoi && dem<cuoi+17){
						Xuat1S(DS[i],a->S,STTX,STT);
						if (dem>cuoi +17) break;
					} if (dem>cuoi +17) break;
				}
			}
		}
		else if(z==27) {NhayTB("ESC: Ve MENU",1,132,40,13,3);	return 27;}
	}
	
}

void XuatSachCua1DG(DauSach DS, int &m){
	int STT=-1, STTX=0,  dem=0; m=0;
	for(PTRS a=DS.ConTroS;a!=NULL;a=a->next)  m++;
	int b=m%17; b=m-b;
	for(PTRS a=DS.ConTroS;a!=NULL;a=a->next){	
		dem++; STT++;
		if(dem>=b+1 && dem<=m){
			Xuat1S(DS,a->S,STTX,STT); STT--; }
	}
}

int Search_MaDS(DauSach DS[], int n, int Ma){
	for(int i=0; i<n; i++)
		if(DS[i].MaDS==Ma) return i;
	return -1;
}

void XoaNhapS(){
	gotoxy(127,26); for(int i=0; i<7;i++) cout<<" ";
	gotoxy(135,29); for(int i=0; i<4;i++) cout<<" ";
	gotoxy(138,32); for(int i=0; i<25;i++) cout<<" ";
}

void NhapS(DauSach DS[], int n){
	
	int Ma, m,z=0,STTX=0; Sach s;
	KhungNhapSach(); KhungSach();
	ThongBao("INSERT: Nhap Moi",1,120,40,17,3); ThongBao("ESC: Ve MENU",3,140,40,17,7);
	while(z!=27){
		z=getch();
		XoaTB(117,5,42,3);
		if(z==82){
			NhayTB("INSERT: Nhap Moi",1,120,40,17,3);
			XoaXuatS();
			XoaNhapS();
			Yescursortype();
			if(NhapMa(Ma,127,26,117,5,10000,99999)==27) {NhayTB("ESC: Ve MENU",3,140,40,17,7); return;}
			 Nocursortype();
			int x=Search_MaDS(DS,n,Ma);
			if(x!=-1){
				XuatSachCua1DG(DS[x],STTX); 
				STTX%=17;
			Yescursortype();
				gotoxy(135,29); if(NhapMa(m,135,29,130,5,0,999)==27) { NhayTB("ESC: Ve MENU",3,140,40,17,7); return;	}
			 Nocursortype();
				int k=0;
				for(PTRS p=DS[x].ConTroS;p!=NULL;p=p->next) k++; //STTX=k%17+1;
				for(int i=k;i<k+m;i++){
					textcolor(7); gotoxy(131,32); for(int j=0;j<25;j++) cout<<char(219); textcolor(0);
					gotoxy(131,32); cout<<i+1<<": ";
					gotoxy(140,32);
					Yescursortype();
					fflush(stdin); if(NhapChuoi(s.ViTri,138,32,130,5,24)==27) { NhayTB("ESC: Ve MENU",3,140,40,17,7); return; }
					Nocursortype();
					s.MaSach=Ma*1000+i+1;
					s.TrangThai=0;
					InsertOrderS(DS[x].ConTroS,s);
					if(STTX==17){ XoaXuatS(); STTX=0;
					}					
					Xuat1S(DS[x],s,STTX,i);i--;
				}
			}
			else ThongBao("Khong tim thay dau sach nay trong thu vien",1,117,5,42,3);
		}
	}
}

void TimSach(DauSach Ds[], int n){
	char S[100]; int a=0, z=0;
	KhungDS();
	VeKhung(115,30,3,50);
	gotoxy(116,31); cout<<"Ten Sach: ";
	ThongBao("INSERT: Tim moi",1,123,37,16,3); ThongBao("ESC: Ve MENU", 1,143,37,16,3);
	while(z!=27){
		z=getch(); XoaTB(122,5,36,3); a=0;
		if(z==82){			
			NhayTB("INSERT: Tim moi",1,123,37,16,3);
			textcolor(7); gotoxy(126,31); for(int i=0;i<37;i++) cout<<char(219);
			gotoxy(126,32); for(int i=0;i<37;i++) cout<<char(219); textcolor(0);
			//XoaTB(117,37,16,3);
			Yescursortype();
			if(NhapChuoi(S,126,31,117,5,37)==27) { NhayTB("ESC: Ve MENU", 1,143,37,16,3); return; }
			Nocursortype();
			for(int i=0;i<n;i++)
				if(stricmp(Ds[i].TenSach,S)==0) { XoaXuat(); Xuat(Ds[i],1,1); a=1;}
			if(a==0) ThongBao("Khong tim thay sach trong thu vien!",1,122,5,36,3);
		}
		else if(z==27) NhayTB("ESC: Ve MENU", 1,143,37,16,3);	
	}
}

typedef struct Node10{
	DauSach DS;
	int so;
	struct Node10 *left;
	struct Node10 *right;
};
typedef struct Node10 *PTR10;

void ThemNode(PTR10 &p, DauSach DS, int  a)
{
	if(p==NULL)
	{
		p=new Node10;
		p->DS=DS;
		p->so = a;
		p->left = p->right = NULL;
	}
	else if(a<p->so) ThemNode(p->left,DS,a);
	else if(a>=p->so) ThemNode(p->right,DS,a);
}


void TaoXuat10Sach(DauSach DS[], int n, PTR10 &First){
	int dem; First =NULL;
	for(int i=0; i<n; i++){
		dem=0;
		for(PTRS p=DS[i].ConTroS;p!=NULL;p=p->next){
			if(p->S.TrangThai==1) dem++;
		}
		ThemNode(First,DS[i],dem);
	}
}
void MuoiSach(PTR10 p, int &muoisach, int &i, int &j){
	if(p!=NULL){
		MuoiSach(p->right,muoisach,i,j);
		muoisach++;	
		if(muoisach <= 10) 	{			
			Xuat(p->DS,i,j);
			gotoxy(109,5+i);cout<<p->so;
		}
		i+=3;j++;
		MuoiSach(p->left,muoisach,i,j);
	}
}

void XuatMuoiSach(DauSach DS[], int n, PTR10 &First){
	TaoXuat10Sach(DS,n,First);
	int dem=0, i=1, j=1;
	KhungDS();
	VeKhung(106,3,50,13);
	gotoxy(107,4); cout<<"SO LUOT MUON";
	VeNgang(107,5,13);
	VeNgang(2,35, 118);
	gotoxy(106,3); cout<<char(GiuaTren);
	gotoxy(106,5); cout<<char(GiuaGiua);
	gotoxy(119,5); cout<<char(GiuaPhai);	
	gotoxy(1,35); cout<<char(TraiDuoi);		
	gotoxy(5,35); cout<<char(GiuaDuoi);	
	gotoxy(13,35); cout<<char(GiuaDuoi);	
	gotoxy(47,35); cout<<char(GiuaDuoi);	
	gotoxy(56,35); cout<<char(GiuaDuoi);		
	gotoxy(82,35); cout<<char(GiuaDuoi);	
	gotoxy(89,35); cout<<char(GiuaDuoi);	
	gotoxy(106,35); cout<<char(GiuaDuoi);	
	gotoxy(119,35); cout<<char(PhaiDuoi);
	
	for(int i=1;i<10;i++){
		gotoxy(107,5+i*3);
		for(int j=0;j<12;j++) cout<<char(196);
	}
	for(int i=0;i<18;i++){
		gotoxy(1,36+i);
		for(int j=0;j<119;j++) cout<<" ";
	}
	MuoiSach(First,dem,i,j);
	ThongBao("ESC: Ve MENU",1,125,32,13,3);
	int z;
	while(1){
		z=getch();
		if(z==27) {			NhayTB("ESC: Ve MENU",1,125,32,13,3); return;	}
	
	}
}


void XoaListSach(DauSach DS[], int n){
	for(int i=0;i<n;i++){
		while(DS[i].ConTroS!=NULL) XoaSach(DS[i].ConTroS);
	}
}

//
//int main()
//{
//	
//}
