#pragma once
#include <iostream>
#include <windows.h>
#include "nhat.h"
#include "menu.h"
#include "muontra.h"
#include "nhapdg.h"
#include <fstream>
int STTDG;
int STTX;
int dem;
using namespace std;

struct DocGia
{
	int MaDG;
	char Ho[40];
	char Ten[10];
	int Phai;
	int TTThe;
	PTR ConTroDG = NULL;
};

struct NodeDG
{
	DocGia DG;
	struct NodeDG *left;
	struct NodeDG *right;
};

typedef struct NodeDG *PTRDG;
PTRDG Tree;


void KhoiTaoCay(PTRDG &root)
{
	root = NULL;
}


int SearchKey(PTRDG &root, int Ma)
{
	PTRDG p;
	p=root;
	while(p!=NULL)
	{
		if(p->DG.MaDG==Ma) return 1;
		if(Ma<p->DG.MaDG)p=p->left;
		else p=p->right;
	}
	return 0;
}

void KhungDG()
{
	gotoxy(32,1); cout<<"BANG THONG TIN DOC GIA";
	int a1=1, b4 = 2, Cot = 54, Hang = 88;
	VeKhung(a1,b4,Cot,Hang);
	
	VeDoc(a1+6,b4,Cot-1);
	VeNgang(a1+1,b4+2,Hang-1);	
	gotoxy(a1,b4+2); cout<<char(GiuaTrai);
	gotoxy(a1+6,b4); cout<<char(GiuaTren);
	gotoxy(a1+6,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+6,Cot+2); cout<<char(GiuaDuoi);
	gotoxy(a1+Hang,b4+2); cout<<char(GiuaPhai);
	gotoxy(a1+1,b4+1); cout<<"STT";
	
	gotoxy(a1+9,b4+1); cout<<"MA DOC GIA";
	VeDoc(a1+22,b4,Cot-1);
	gotoxy(a1+22,b4); cout<<char(GiuaTren);
	gotoxy(a1+22,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+22,Cot+2); cout<<char(GiuaDuoi);
	
	gotoxy(a1+36,b4+1); cout<<"HO VA TEN";
	
	gotoxy(a1+59,b4+1); cout<<"PHAI";
	VeDoc(a1+56,b4,Cot-1);
	gotoxy(a1+56,b4); cout<<char(GiuaTren);
	gotoxy(a1+56,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+56,Cot+2); cout<<char(GiuaDuoi);	
	
	gotoxy(a1+71,b4+1); cout<<"TRANG THAI THE";
	VeDoc(a1+65,b4,Cot-1);
	gotoxy(a1+65,b4); cout<<char(GiuaTren);
	gotoxy(a1+65,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+65,Cot+2); cout<<char(GiuaDuoi);
	for(int i=6;i<55;i+=2){
		gotoxy(a1+1,i); for(int j=0;j<87;j++) {
			if(j==5 ||j==21 || j==55 || j==64) gotoxy(2+j+1,i);
			else cout<<char(196);
		}
	}	
}


void XoaKhung(){
	for(int i=0;i<52;i++){
		if(i%2==0);
		else{
	 		gotoxy(2,4+i); for(int j=0;j<5;j++) cout<<" ";	
			gotoxy(8,4+i); for(int j=0;j<15;j++) cout<<" ";	
	 		gotoxy(24,4+i); for(int j=0;j<33;j++) cout<<" ";	
	 		gotoxy(58,4+i); for(int j=0;j<8;j++) cout<<" ";	
	 		gotoxy(67,4+i); for(int j=0;j<22;j++) cout<<" ";			
		}		
	}
}

void KhungNhapDG()
{
	VeKhung(110,18,17,55);
	gotoxy(112,20); cout<<"Ma Doc Gia: ";
	gotoxy(112,23); cout<<"Ho & Ten Dem: ";
	gotoxy(112,26); cout<<"Ten: "; 
	gotoxy(112,29); cout<<"Phai(1:Nam, 0:Nu): ";
	gotoxy(112,32); cout<<"Trang Thai The(1:Binh Thuong, 0:Bi Khoa): ";
}


void XoaNhapDG()
{
	textcolor(7);
	for(int i=0; i<=7;i++)
	{
		gotoxy(126,19+i);
		for(int j=0;j<39;j++) cout<<char(219);
	}
	for(int i=0; i<=5;i++)
	{
		gotoxy(154,27+i);
		for(int j=0;j<10;j++) cout<<char(219);
	}
	textcolor(0);
}



void InDocGiaX(DocGia A, int &dem, int &STTX)
{
	dem++;
	STTX++; int i=dem*2-1;
	gotoxy(2,4+i); cout<<STTX;
	gotoxy(8,4+i); cout<<A.MaDG;
	gotoxy(24,4+i); cout<<A.Ho<<" "; int x = wherex(), y = wherey(); gotoxy(x,y); cout	<<A.Ten;
	gotoxy(60,4+i); if(A.Phai==0) {cout<<"Nu";} else cout<<"Nam";
	gotoxy(67,4+i); if(A.TTThe==0) { cout<<"Bi khoa";} else cout<<"Binh thuong";
}


void InorderX(PTRDG p,int &STTX, int &demso, int dau, int cuoi) //Duyet de qui
{
	if(p!=NULL)
	{
		InorderX(p->left,STTX,demso,dau,cuoi);
		demso++;
		if(demso>=dau && demso<=cuoi)	InDocGiaX(p->DG,dem,STTX); 
		if(demso==cuoi) return ;
		InorderX(p->right,STTX,demso,dau,cuoi);
	}
}

void Inorderdem(PTRDG p, int &n) //Duyet de qui
{
	if(p!=NULL)
	{
		Inorderdem(p->left,n);
		 n++;
		Inorderdem(p->right,n);
	}
}

void InDSDocGiaX(PTRDG root, int dau, int cuoi)
{ 
	int n=0,demso=0;  
	Inorderdem(root,n); 
	STTX=0; dem=0;
	char iox;
	ThongBao("UP: Trang truoc",1,110,40,16,3); ThongBao("DOWN: Trang sau",1,130,40,16,3);
	ThongBao("ESC: Ve MENU",2,120,43,16,3);
	InorderX(root,STTX,demso,dau,cuoi);
	do{
		iox = getch();
		if(iox==72) NhayTB("UP: Trang truoc",1,110,40,16,3);
		if(iox==80) NhayTB("DOWN: Trang sau",1,130,40,16,3);
		if(iox==27) NhayTB("ESC: Ve MENU",2,120,43,16,3);
		if(iox == 80 && cuoi<n){ dem=0;
			XoaKhung();  demso=0; dau=cuoi+1; cuoi+=26;  if(cuoi>n) cuoi = n;
			InorderX(root,STTX,demso,dau,cuoi);
		}
		else if(iox ==72 && cuoi >26){
			XoaKhung(); demso=0; dem=0;
			if(cuoi%26==0) { 
				STTX=(STTX/26-2)*26; dau=(cuoi/26-1)*26-25; cuoi = (cuoi/26-1)*26;
				InorderX(root,STTX,demso,dau,cuoi);
			}
			else{
				dau=(cuoi/26)*26-25; cuoi=(cuoi/26)*26; 			
				STTX=(STTX/26-1)*26; InorderX(root,STTX,demso,dau,cuoi);
			}
		}
	} while(iox!=27);
}


void In1DocGia(DocGia A, int &STTDG)
{	int i; 
	if(STTDG%26==0) { i= 27*2+1;
		gotoxy(2,i);cout<<STTDG;  
	}
	else if((STTDG-1)%26==0){
		XoaKhung();
		i= 5;
		gotoxy(2,i);cout<<STTDG;  ;
	}
	else {	
	i=(STTDG%26)*2+3;
		gotoxy(2,i); cout<<STTDG; 
 		
	}
	gotoxy(8,i); cout<<A.MaDG;
	gotoxy(24,i); cout<<A.Ho<<" ";  cout	<<A.Ten;
	gotoxy(60,i); if(A.Phai==0) {cout<<"Nu";} else cout<<"Nam";
	gotoxy(67,i); if(A.TTThe==0) { cout<<"Bi khoa";} else cout<<"Binh thuong";
	STTDG++;
}


void Inorder(PTRDG p, int MA, PTRDG &q) //Duyet de qui
{
	if(p!=NULL)
	{
		Inorder(p->left, MA,q);
		if(p->DG.MaDG==MA)	q=p; 
		Inorder(p->right, MA,q);
	}
}
int NhapDGS(PTRDG Tree, int &ma, char Ten[], char Ho[], int &gt, int &tt);
int NhapThongTinDG(DocGia &DG, PTRDG Tree)
{	
	PTRDG p=NULL;int a = 1; int Ma;
	
		textcolor(7);
		gotoxy(128,20); for(int i=0; i<35; i++) cout<<char(219);
		textcolor(0);

		if(NhapDGS(Tree, Ma,DG.Ten, DG.Ho, DG.Phai, DG.TTThe)!=0) ;
		else return 27; DG.MaDG=Ma;
}

void ThemDocGia(PTRDG &p, DocGia DG)
{
	if(p==NULL)
	{
		p=new NodeDG;
		p->DG = DG;
		p->left = p->right = NULL;
	}
	else if(DG.MaDG<p->DG.MaDG) ThemDocGia(p->left,DG);
	else if(DG.MaDG>p->DG.MaDG) ThemDocGia(p->right,DG);
}

void CreateTree(PTRDG &root)
{	
	
	int z = 0, n,demso,m; ThongBao("ESC: Ve MENU",2,140,37,17,3); 
	DocGia A;
	XoaKhung();
	dem=0; demso=0;  Inorderdem(root,n); m=n+1;
	if(n%26==0) n-=26; STTX = (n/26)*26;
	InorderX(root,STTX,demso,(n/26)*26+1,n);
	do
	{
		gotoxy(120,36);ThongBao("INSERT: Nhap moi",1,120,37,17,3); 
		
		z = getch();
	
		if(z==82){	
			NhayTB("INSERT: Nhap moi",1,120,37,17,3);
			XoaTB(120,37,17,3);
				if(NhapThongTinDG(A,root)==27) z=27;
				else{					 	
					ThemDocGia(root,A);
					In1DocGia(A, m);			
					XoaNhapDG();
				}
		}
		if(z==27) NhayTB("ESC: Ve MENU",2,140,37,17,3);
	} while(z!=27);
}


void NhapDocGia(PTRDG &root)
{
	clrscr();	
	KhungDG();		
	KhungNhapDG();
	CreateTree(root);
}

void XuatMuon(PTRDG p, int &STT, int &dem, int &i, int &dau, int &cuoi){ 
	if(p!=NULL){
		XuatMuon(p->left,STT, dem, i, dau, cuoi);
		for(PTR a = p->DG.ConTroDG; a!=NULL; a=a->next){
			i++; 
			if(i>=dau && i<=cuoi){	XuatM(p->DG.MaDG,a->MT,dem,STT); dem++; STT++;}
			if(i==cuoi) return;
		}
		XuatMuon(p->right,STT,dem,i,dau,cuoi);
	}
}


void DemDS(PTRDG p, int &n){ 
	if(p!=NULL){
		DemDS(p->left,n);
		for(PTR a = p->DG.ConTroDG; a!=NULL; a=a->next) n++;
		DemDS(p->right,n);
	}
}

void XuatDSMuon(PTRDG root, int &STT){
	int dem=0,dau=1, cuoi=24, i=0;
	XuatMuon(root,STT,dem,i,dau,cuoi);
}

void NhapDSMT(PTRDG &root, DauSach DS[], int n)
{
	PTR q;
	KhungMuonTra(); 
	KhungNhapMT();
	int MA, KT3=0, KTttthe, MT1=0, m=0, STT=1, z; DemDS(root,m);
	PTRDG p; int dem=1,i=0,dau=1, cuoi=24; PTRDG as;
	XuatMuon(root,STT,dem,i,dau,cuoi);
	
	while(1){
		MT1=0, MA=0;
		XoaTB(134,40,17,3); XoaTB(105,40,20,3);
		ThongBao("UP: Trang Truoc",1,105,44,17,3); ThongBao("DOWN: Trang Sau",1,135,44,17,3);
		gotoxy(105,45);ThongBao("INSERT: Nhap moi",1,105,40,17,3); ThongBao("ESC: Ve MENU",1,135,40,17,3);
		z = getch();
		if(z==27) {NhayTB("ESC: Ve MENU",1,135,40,17,3); return;	}
		if(z==72) NhayTB("UP: Trang Truoc",1,105,44,17,3); 
		if(z==80) NhayTB("DOWN: Trang Sau",1,135,44,17,3);
		if(z==82){	
			XoaTB(105,44,17,3); XoaTB(135,44,17,3);
			gotoxy(114,24); for(int i=0; i<30;i++) cout<<" ";
			NhayTB("INSERT: Nhap moi",1,105,40,17,3); 
			ThongBao("MA DOC GIA co 6 chu so",1,107,15,23,3);
			Yescursortype();
			if(NhapMa(MA,114,24,117,5,100000,999999)==27) {NhayTB("ESC: Ve MENU",1,135,40,17,3); return;	}
			Nocursortype();
			XoaTB(107,15,23,3);
			p=NULL;
			Inorder(root, MA,p);
			if(p!=NULL){
			if(p->DG.TTThe==0) {
				ThongBao("The Muon Sach Da Het Han!",1, 107,5,27,3); 				
				Sleep(500); XoaTB(107,5,27,3);
			}
			else{
				PTR q;
				MuonTra A;
				do{
					KT3=0;
					for(q=p->DG.ConTroDG; q!=NULL;q=q->next)	if(q->MT.NgayTra.Nam==0) KT3++;	
					XoaTB(105,40,17,3); XoaTB(135,40,17,3);
					ThongBao("ESC: Tro Lai",1,134,40,17,3);		ThongBao("F1: Nhap Sach Moi",1,105,40,20,3);
					z = getch();
					if(z==59){	
						XoaNhapMT();
						NhayTB("F1: Nhap Sach Moi",1,105,40,20,3);
						if(KT3==3){ ThongBao("Mot Doc Gia Khong The Muon Qua 3 Cuon!",1, 107,5,40,3);Sleep(500);XoaTB(107,5,40,3);	}
						else{				
							XoaTB(105,45,16,3);			
							if(NhapMuon(A, STT,DS,n)==27) {NhayTB("ESC: Tro Lai",1,134,40,17,3); z=27;}			
							else{
								XoaXuatMT();
								dem=1; i=0; dau=m/24; dau=dau*24 +1; cuoi =m; STT=dau;
								XuatMuon(root,STT,dem,i,dau, cuoi); 
								Insert_Order(p->DG.ConTroDG,A);
								XuatM(p->DG.MaDG, A,dem, STT);			m++;
								XoaNhapMT();
							}
						}
					}
					if(z==27) NhayTB("ESC: Tro Lai",1,134,40,17,3);
					
				} while(z!=27);
			}
		}
		else {
			ThongBao("Khong Tim Thay Doc Gia Nay!",1, 107,5,28,3); 	
			Sleep(500); XoaTB(107,5,28,3);
		}
	
		} else if(z==27) {NhayTB("ESC: Ve MENU",1,135,40,17,3); return;}
		
		else if(z==72 && STT>24) { XoaXuatMT();
			if(STT%24==0) STT-=47;
			else {
				dau = STT%24;
				STT=STT-(23+dau); 
			}
			dem=1; i=0;dau=STT; cuoi =STT+23;
			XuatMuon(root,STT,dem,i,dau, cuoi); STT--;
		}	
		else if(z==80 && STT!=m){
			if(STT%24==0) STT++; 
			XoaXuatMT(); dem=1; i=0;dau=STT; cuoi =STT+23;
			XuatMuon(root,STT,dem,i,dau, cuoi);  STT--;
		}
	}
}

void TraSach(PTRDG &root, DauSach DS[], int n){
	int MaDG, MT2, MT3, z,i; int MaS; PTRDG p; PTR q; int dem=1,STT=1,a=0,dau=1, cuoi = 24;
	KhungTra(); int m=0; DemDS(root,m);
	KhungMuonTra(); int j=1;
	XuatMuon(root,STT,dem,a,dau,cuoi);
	gotoxy(102,31); cout<<"MA DOC GIA: ";
	gotoxy(102,32); cout<<"MA SACH: ";
	while(1){
		ThongBao("UP: Trang Truoc",3,105,43,21,3); ThongBao("DOWN: Trang Sau",3,130,43,21,3);
		XoaTB(105,39,20,3);  XoaTB(135,39,13,3); 
		textcolor(7);	gotoxy(115,31); for(int j=0; j<40;j++) cout<<char(219);		textcolor(0);		
		ThongBao("ESC: Ve MENU",5,130,39,21,3);
		ThongBao("INSERT: Nhap Doc Gia",1,105,39,21,3);		
		gotoxy(115,32); for(int j=0; j<10;j++) cout<<" ";
		z=getch();		
		if(z==72) NhayTB("UP: Trang Truoc",3,105,43,21,3); 
		if(z==80) NhayTB("DOWN: Trang Sau",3,130,43,21,3);
		if(z==27) { NhayTB("ESC: Ve MENU",5,130,39,21,3); return; }
		if(z==82){
			NhayTB("INSERT: Nhap Doc Gia",1,105,39,21,3);
			XoaTB(105,43,21,3); XoaTB(130,43,21,3);
			Yescursortype();
		if(NhapMa(MaDG,115,31,117,5,100000,999999)==27) { NhayTB("ESC: Ve MENU",5,130,39,21,3); return; }
		Nocursortype();
		p=NULL;
		Inorder(root, MaDG,p);
		if(p==NULL){
			ThongBao("Khong Tim Thay Doc Gia Nay!",1, 107,5,28,3); 
			Sleep(500); XoaTB(107,5,28,3);
		}
		else{
			do{		
				XoaTB(105,39,21,3); XoaTB(135,39,13,3);
				ThongBao("F1: Nhap Sach Tra",1,105,39,20,3);
				ThongBao("ESC: Ve MENU",5,130,39,21,3);
				z = getch();
				if(z==59){	NhayTB("F1: Nhap Sach Tra",1,105,39,20,3);
					gotoxy(115,32); for(int j=0; j<40;j++) cout<<" ";
					Yescursortype();
					 if(NhapMa(MaS,115,32,117,5,10000000,99999999)==27) { NhayTB("ESC: Tro Lai",5,130,39,21,3); z=27; }
					 Nocursortype();
					q=Search_infor(p->DG.ConTroDG,MaS,dem);
					if(q==NULL || q->MT.TrangThai==1){
						ThongBao("Doc Gia Khong Muon Sach Nay!",1, 107, 5,30,3); 
						Sleep(500); XoaTB(107,5,30,3);
					}
					else{							
						NhapTra(q->MT);
						Yescursortype();
						if(NhapMa(i,138,34,117,5,1,2)==27) { NhayTB("ESC: Tro Lai",5,130,39,21,3); z=27; }
						Nocursortype();
						if(i==1) q->MT.TrangThai=2;
						else	q->MT.TrangThai = 1; 
						j=1;
						for(int l=0;l<n;l++)
							for(PTRS k=DS[l].ConTroS;k!=NULL;k=k->next)
								if(k->S.MaSach==MaS) k->S.TrangThai=0;
						//textcolor(7); gotoxy(56,7+dem*2); 	for(int i=0; i<10;i++) cout<<char(219);	textcolor(0);
						XoaXuatMT();
						XuatM(p->DG.MaDG,q->MT,1,1);						
					} XoaTB(105,39,17,3);
				}
				if(z==27) NhayTB("ESC: Tro Lai",5,130,39,21,3);
			} while(z!=27);
		}
		}
		else if(z==72 && STT>24) { XoaXuatMT();
			if(STT%24==0) STT-=47;
			else {
				dau = STT%24;
				STT=STT-(23+dau); 
			}
			dem=1; i=0;dau=STT; cuoi =STT+23;
			XuatMuon(root,STT,dem,i,dau, cuoi); STT--;
		}	
		else if(z==80 && STT!=m){
			if(STT%24==0) STT++; 
			XoaXuatMT(); dem=1; i=0;dau=STT; cuoi =STT+23;
			XuatMuon(root,STT,dem,i,dau, cuoi);  STT--;
		}
	} 
}


void XuatMuonDG(PTRDG &root)
{
	PTRDG p=NULL; int MaDG, MT4;
	KhungMuonTraDG(); ThongBao("ESC: Ve MENU",3,135,35,17,3); ThongBao("INSERT: Nhap Moi",1,112,35,17,3);
	VeKhung(112, 30, 2,40);
	gotoxy(113, 31); cout<<"MA DOC GIA: ";
	do{
		MT4=getch();
		XoaXuatMTDG();
		if(MT4==27) NhayTB("ESC: Ve MENU",3,135,35,17,3); 
		else if(MT4==82){
			textcolor(7); gotoxy(125, 31); for(int i=0;i<8;i++) cout<<char(219); textcolor(0);
			NhayTB("INSERT: Nhap Moi",1,112,35,17,3);
			Yescursortype();
			if(NhapMa(MaDG,125,31,117,5,100000,999999)==27) { NhayTB("ESC: Ve MENU",3,135,35,17,3); return; }
			Nocursortype();
			Inorder(root,MaDG,p);
			if(p==NULL){
			ThongBao("Khong Tim Thay Doc Gia Nay!",1, 117,5,28,3); 
			Sleep(500); XoaTB(117,5,28,3);
		}
		else 	XuatDSMT(p->DG.ConTroDG,p->DG.MaDG); 
	
		}
	} while( MT4!=27);
	
}

void XoaMuonTra(PTRDG &p){
	if(p!=NULL){
		XoaMuonTra(p->left);
		while(p->DG.ConTroDG!=NULL)		Clear_List(p->DG.ConTroDG);
		XoaMuonTra(p->right);
	}
}
//
//void XoaCay(PTRDG &p){
//	if(p!=NULL){
//		XoaCay(p->left);
//		
//		Xoa
//	}
//}

void ThemDocGiaGhi(PTRDG &p, DocGia DG)
{
	if(p==NULL)
	{
		p=new NodeDG;
		p->DG = DG;
		//p->DG.ConTroDG = new NodeMT;
		KhoiTao(p->DG.ConTroDG);
		p->DG.ConTroDG=DG.ConTroDG;
		p->left = p->right = NULL;
	}
	else if(DG.MaDG<p->DG.MaDG) ThemDocGiaGhi(p->left,DG);
	else if(DG.MaDG>p->DG.MaDG) ThemDocGiaGhi(p->right,DG);
}
void ghi3(PTR p, ofstream &m){
	m<<p->MT.MaSach; m<<",";
	m<<p->MT.NgayMuon.Nam; m<<",";
	m<<p->MT.NgayMuon.Thang; m<<",";
	m<<p->MT.NgayMuon.Ngay; m<<",";		
	m<<p->MT.NgayTra.Nam; m<<",";			
	m<<p->MT.NgayTra.Thang; m<<",";			
	m<<p->MT.NgayTra.Ngay; m<<",";
	m<<p->MT.TrangThai;	m<<",";
}
void ghi1(PTRDG p, ofstream &g){
	
	if(p!=NULL){
		int i=0;
		for(PTR q=p->DG.ConTroDG;q!=NULL;q=q->next)	i++;
		g<<i; g<<","; i=0;
		for(PTR q=p->DG.ConTroDG;q!=NULL;q=q->next)	ghi3(q,g);
		g<<p->DG.MaDG; g<<",";
		g<<p->DG.Ho; g<<",";
		g<<p->DG.Ten; g<<",";
		g<<p->DG.Phai; g<<",";
		g<<p->DG.TTThe; g<<endl;
		ghi1(p->right,g);
		ghi1(p->left,g);
	}
}
void ghi(PTRDG p){
	ofstream g("D:/docgia.txt", ios::out);
	ghi1(p,g);
	g.close();
}
void doc(PTRDG &p, int &i, PTR &First){
	DocGia a;  i=0; string f; MuonTra b;
	ifstream d("docgia.txt", ios::in);
	while(!d.eof()){
		getline(d,f); i++;
	} i--;  int m=i;
	d.close();
	ifstream doc("docgia.txt", ios::in);
	while(m>0){	
		string madocgia, ho,ten,phai,trangthaithe, sosach;		KhoiTao(a.ConTroDG);
		getline(doc,sosach,',');int n =atoi(sosach.c_str()); 
		while(n>0){	
			
			string masach, ngay, thang, nam,ngay1, thang1, nam1,trangthai;
			
			getline(doc,masach,',');b.MaSach=atoi(masach.c_str()); 
					
			getline(doc,nam,',');  b.NgayMuon.Nam =atoi(nam.c_str());
		
			getline(doc,thang,','); b.NgayMuon.Thang =atoi(thang.c_str());
		
			getline(doc,ngay,','); b.NgayMuon.Ngay =atoi(ngay.c_str());
		
			getline(doc,nam1,','); b.NgayTra.Nam =atoi(nam1.c_str());
		
			getline(doc,thang1,','); b.NgayTra.Thang =atoi(thang1.c_str());
		
			getline(doc,ngay1,',');  b.NgayTra.Ngay =atoi(ngay1.c_str());
		
			getline(doc,trangthai,',');  b.TrangThai=atoi(trangthai.c_str());
		
			Insert_Order(First, b); 
			Insert_Order(a.ConTroDG, b);
			n--;
		}
		
		getline(doc,madocgia,','); a.MaDG=atoi(madocgia.c_str()); 
		
		getline(doc,ho,','); for(int j=0;j<ho.length();j++) a.Ho[j]=ho[j];
		 a.Ho[ho.length()]='\0'; 
		 
		getline(doc,ten,','); for(int j=0;j<ten.length();j++) a.Ten[j]=ten[j]; 
		a.Ten[ten.length()]='\0'; 
		
		getline(doc,phai,',');a.Phai=atoi(phai.c_str()); 
		
		getline(doc,trangthaithe);a.TTThe=atoi(trangthaithe.c_str()); 
		ThemDocGiaGhi(p,a);
		m--;
	}
	doc.close();
}


int NhapDGS(PTRDG Tree, int &ma, char Ten[], char Ho[], int &gt, int &tt){
	
	 ThongBao("UP: Nhap gia tri tren",2,113,44,24,3); ThongBao("DOWN: Nhap gia tri duoi",1,139,44,24,3);
	                          ThongBao("ENTER: Nhap xong",1,130,47,17,3);
	int x=128, x1=154, y=23;
	int n=0; PTRDG d;
	int c=-1, i=-1, j=-1; gt=2; tt=2; ma=0;
	int k=-1, l=-1;
	int s[20], u[20], b[20];
	Yescursortype();
	while(n==0 ){
		c++; gotoxy(x+c,y-3); b[c]=getch();
		
		if(b[c]>=48 && b[c]<=57){ 
			b[c]-=48;
			ma=ma*10+ b[c]; 
			if(ma>999999) {c--; ma/=10; continue;}
			else cout<<b[c];
		}
		else if(b[c]==8){
			ma/=10;
			gotoxy(x+c-1, y-3); cout<<" ";							
			if(c>0) c-=2;
			else c--;  
			
		}
		else if(b[c]==13){
			c--;
			NhayTB("ENTER: Nhap xong",1,130,47,17,3);
			
			d=NULL;
			Inorder(Tree,ma,d);
			if(d!=NULL)	{ThongBao("Ma Doc Gia Bi Trung!", 1, 117,5,21,3); Sleep(300); XoaTB(117,5,21,3); continue;}
			if(ma<100000 || ma>999999){ ThongBao("Ma Doc Gia phai co 6 chu so!",1,120,5,29,3); Sleep(300); XoaTB(120,5,29,3); continue;}
			if(Ho[0]<97 || Ho[0]>122)	{ ThongBao("Chua nhap Ho!", 1, 127,5,15,3); Sleep(300); XoaTB(127,5,15,3);continue;}	
			if(Ten[0]<97 || Ten[0]>122){ ThongBao("Chua nhap Ten!", 1, 127,5,15,3);Sleep(300); XoaTB(127,5,15,3); continue;}	
			if(gt==2) { ThongBao("Chua nhap Phai!", 1, 127,5,16,3); Sleep(300); XoaTB(127,5,16,3);continue;}	
			if(tt==2) { ThongBao("Chua nhap Trang Thai The!", 1, 122,5,26,3);Sleep(300); XoaTB(122,5,26,3); continue;}	
			n=1; break;
		}						
		else if(b[c]==72){
			 c--; NhayTB("UP: Nhap gia tri tren",2,113,44,24,3);			 
		}
		else if(b[c]==27){ c--; NhayTB("ESC: Ve MENU",2,140,37,17,3); return 0;}
		else if(b[c]==80){ c--;
			d=NULL;
			Inorder(Tree,ma,d);
			if(d!=NULL)	{ThongBao("Ma Doc Gia Bi Trung!", 1, 117,5,21,3); Sleep(300); XoaTB(117,5,21,3);	continue;}
			else if(ma<100000 || ma>999999){ ThongBao("Ma Doc Gia phai co 6 chu so!",1,120,5,29,3); Sleep(300); XoaTB(120,5,29,3); continue;}
			else if(ma==0) { ThongBao("Chua nhap MaDG!", 1, 127,5,16,3);Sleep(300); XoaTB(127,5,16,3); continue;}
					NhayTB("DOWN: Nhap gia tri duoi",1,139,44,24,3);
		
	
	while(n==0){
		
		i++; 
		gotoxy(x+i,y); Ho[i]=getch();
		
		if((Ho[i]>=97 && Ho[i]<=122) || Ho[i]==32)
		{
			if(i>0 && Ho[i]==32){
				if(Ho[i-1]==32){
					i--;
					ThongBao("Ten khong co 2 khoang trang lien tiep!", 1, 117,5,40,3); Sleep(300); XoaTB(117,5,40,3); continue;
				}
			}
			else if(i==0 && Ho[i]==32){			
				i--;
				ThongBao("Bat dau ten khong phai khoang trong!", 1, 119,5,37,3);Sleep(300); XoaTB(119,5,37,3); continue;
			}
			else cout<<Ho[i];		
	 }
		else if(Ho[i]==8){ Ho[i]=32;	Ho[i-1]=32;
			gotoxy(x+i-1,y); cout<<" "; 
			if(i>0) i-=2;
			else i--;
		}
		else if(Ho[i]==13) {	Ho[i]=32;		
			i--; ThongBao("ENTER: Nhap xong",1,130,47,17,3);
			if(Ho[i]==32)	i--;
			else if(Ho[0]<97 || Ho[0]>122)	{ ThongBao("Chua nhap Ho!", 1, 127,5,15,3); Sleep(300); XoaTB(127,5,15,3);continue;}	
			else if(Ten[0]<97 || Ten[0]>122){ ThongBao("Chua nhap Ten!", 1, 127,5,15,3);Sleep(300); XoaTB(127,5,15,3); continue;}	
			else if(gt==2) { ThongBao("Chua nhap Phai!", 1, 127,5,16,3); Sleep(300); XoaTB(127,5,16,3);continue;}	
			else if(tt==2) { ThongBao("Chua nhap Trang Thai The!", 1, 122,5,26,3);Sleep(300); XoaTB(122,5,26,3); continue;}	
			n=1; break;
		}
		else if(Ho[i]==72) {i--; NhayTB("UP: Nhap gia tri tren",2,113,44,24,3); break;}			
		else if(Ho[i]==27){
			i--; NhayTB("ESC: Ve MENU",2,140,37,17,3); 
			return 0;
		}
		else if(Ho[i]==80)
		{			Ho[i]=32;
			i--; NhayTB("DOWN: Nhap gia tri duoi",1,139,44,24,3);
			if(Ho[0]<97 || Ho[0]>122)	{ ThongBao("Chua nhap Ho!", 1, 127,5,15,3); Sleep(300); XoaTB(127,5,15,3);continue;}			
			if(Ho[i]==32)	i--;
			
	while(n==0){
				j++; 
				gotoxy(x+j,y+3); Ten[j]=getch();
				
				if(Ten[j]>=97 && Ten[j]<=122 || Ten[j]==32){
					if(j>0 && Ten[j]==32){
						if(Ten[j-1]==32){
							j--;
							ThongBao("Ten khong co 2 khoang trang lien tiep!", 1, 117,5,40,3);Sleep(300); XoaTB(117,5,40,3); continue;
						}
					}
					else if(j==0 && Ten[j]==32){					
						j--;
						ThongBao("Bat dau ten khong phai khoang trong!", 1, 119,5,37,3); Sleep(300); XoaTB(119,5,37,3); continue;
					}
					else	cout<<Ten[j];
				}
				else if(Ten[j]==8){ Ten[j]=32;	Ten[j-1]=32;
							gotoxy(x+j-1,y+3); cout<<" "; 
							if(j>0) j-=2;
							else j--;
				}
				else if(Ten[j]==13) {
					NhayTB("ENTER: Nhap xong",1,130,47,17,3);
					j--;
					if(Ten[j]==32)	j--;
					if(Ten[0]<97 || Ten[0]>122){ ThongBao("Chua nhap Ten!", 1,127,5,15,3); Sleep(300); XoaTB(127,5,15,3);continue;}	
					if(gt==2) { ThongBao("Chua nhap Phai!", 1, 127,5,16,3); Sleep(300); XoaTB(127,5,16,3);continue;}	
					if(tt==2) { ThongBao("Chua nhap Trang Thai The!", 1, 122,5,26,3);Sleep(300); XoaTB(122,5,26,3); continue;}	
					n=1; break;
				}
				else if(Ten[j]==72){	
					j--; NhayTB("UP: Nhap gia tri tren",2,113,44,24,3);
					break;
				}
				else if(Ten[j]==27){
					j--; NhayTB("ESC: Ve MENU",2,140,37,17,3); 
					return 0;
				}
				else if(Ten[j]==80)
				{Ten[j]=32;
					j--; NhayTB("DOWN: Nhap gia tri duoi",1,139,44,24,3);
					if(Ten[0]<97 || Ten[0]>122)	{ ThongBao("Chua nhap Ten!", 1, 127,5,15,3);Sleep(300); XoaTB(127,5,15,3); continue;}
					if(Ten[j]==32) j--;
	while(n==0){
						
						k++;
						gotoxy(x1+k,y+6); s[k]=getch();
						
						if(k==0 && (s[k]==48 || s[k]==49)){
							gt=s[0]-48;
							if(gt!=1 && gt !=0) continue;
							else cout<<gt;
						}
						else if(s[k]==8){
							gotoxy(x1+k-1, y+6); cout<<" ";
							
							if(k>0) k-=2;
							else k--;
							if(k==-1) gt=2;
						}
						else if(s[k]==13){
							k--;
							NhayTB("ENTER: Nhap xong",1,130,47,17,3);
							if(gt==2) { ThongBao("Chua nhap Phai!", 1, 127,5,16,3);Sleep(300); XoaTB(127,5,16,3); continue;}	
							if(tt==2) { ThongBao("Chua nhap Trang Thai The!", 1, 122,5,26,3); Sleep(300); XoaTB(122,5,26,3); continue;}		
							n=1; break;
						}
						
						else if(s[k]==72){
							 k--; NhayTB("UP: Nhap gia tri tren",2,113,44,24,3);
							 break;
						}						
						else if(s[k]==27){
							k--;NhayTB("ESC: Ve MENU",2,140,37,17,3);  
							return 0;
						}
						else if(s[k]==80){
						
							k--;	NhayTB("DOWN: Nhap gia tri duoi",1,139,44,24,3);
							if(gt==2) { ThongBao("Chua nhap Phai!", 1, 127,5,16,3); Sleep(300); XoaTB(127,5,16,3); continue;}							
	while(n==0){
								l++;
								gotoxy(x1+l,y+9); u[l]=getch();
						
								if(l==0 && (u[l]==48 || u[l]==49)){
									tt=u[0]-48;
									if(tt!=1 && tt !=0) continue;
									else cout<<tt;
								}
								else if(u[l]==8){
									gotoxy(x1+l-1, y+6); cout<<" ";							
									if(l>0) l-=2;
									else l--;
									if(l==-1) tt=2;
								}
								else if(u[l]==13){ 
									l--; NhayTB("ENTER: Nhap xong",1,130,47,17,3);
									if(tt==2) { ThongBao("Chua nhap Phai!", 1, 127,5,16,3); Sleep(300); XoaTB(127,5,16,3); continue;}
									n=1; break;
									
								}
								else if(u[l]==72){l--; NhayTB("UP: Nhap gia tri tren",2,113,44,24,3);	 break;	}
								else if(u[l]==80) {l--;NhayTB("DOWN: Nhap gia tri duoi",1,139,44,24,3);	}
								else if(u[l]==27){ l--; NhayTB("ESC: Ve MENU",2,140,37,17,3);  return 0; }
								else l--;
							}
							
						}
						
						else k--;	
					
				}
				
		}
				else j--;
		}
		}	
		else i--;
	} 
}
	else c--;
}
Nocursortype();
	XoaTB(113,44,24,3); XoaTB(139,44,24,3); XoaTB(130,47,17,3);
	Ho[i+1]='\0'; Ten[j+1]='\0';
	ChuanHoa(Ho,i+1); ChuanHoa(Ten,j+1);
	textcolor(7);
	gotoxy(114,43); for(int a=0;a<50;a++) cout<<char(219);
	textcolor(0);
}




