#pragma once
#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <iomanip>
#include "nhat.h"


#define LEFT 	75
#define RIGHT 	77
#define UP 		72
#define DOWN 	80
#define ENTER 	13


#define KEY_HOME        71
#define KEY_UP          72
#define KEY_PGUP        73
#define KEY_LEFT        75
#define KEY_CENTER      76
#define KEY_RIGHT       77
#define KEY_END         79
#define KEY_DOWN        80
#define KEY_PGDN        81
#define KEY_INSERT      82
#define KEY_DELETE      83
#define KEY_F1          59
#define KEY_F2          60
#define KEY_F3          61
#define KEY_F4          62
#define KEY_F5          63
#define KEY_F6          64
#define KEY_F7          65
#define KEY_F8          66
#define KEY_F9          67


#define MAUNEN	14
#define MAUCHU	0



#define Dung 186
#define Ngang 205
#define GiuaTren 203
#define GiuaDuoi 202
#define TraiTren 201
#define TraiDuoi 200
#define PhaiTren 187
#define PhaiDuoi 188
#define GiuaTrai 204
#define GiuaPhai 185
#define GiuaGiua 206


using namespace std;


typedef char str[75];
str ThaoTac[13] = {
	"Nhap Doc Gia." ,
	"In Danh Sach Doc Gia." ,
	"Nhap Dau Sach." ,
	"In Danh Sach Cac Dau Sach Trong Thu Vien." ,
	"Nhap Sach.",
	"In Danh Sach Cac Sach Trong Thu Vien.",
	"Tim Thong Tin Sach Dua Vao Ten Sach.",
	"Muon Sach.",
	"Tra Sach.",
	"Liet Ke Danh Sach Cac Sach Ma Mot Doc Gia Dang Muon.",
	"In Danh Sach Doc Gia Muon Sach Qua Han Theo Thu Tu Thoi Gian Giam Dan.",
	"In 10 Sach Co So Luot Muon Nhieu Nhat.",
	"Thoat."
};

void ChuMenu(int x, int y, int HANG)
{	
	textcolor(12);
	gotoxy(HANG/2+x-7,y-4); cout<<char(TraiTren); cout<<char(PhaiTren)<<char(TraiTren)<<char(PhaiTren)<<" "
								<<char(TraiTren)<<char(Ngang)<<" "
								<<char(TraiTren)<<char(Ngang)<<char(PhaiTren)<<" "
								<<char(GiuaTren)<<" "<<char(GiuaTren);
	
	gotoxy(HANG/2+x-9,y-3);	cout<<char(15)<<" "<<char(Dung)<<char(TraiDuoi)<<char(PhaiDuoi)<<char(Dung)<<" "
								<<char(GiuaTrai)<<char(Ngang)<<" "
								<<char(Dung)<<" "<<char(Dung)<<" "
								<<char(Dung)<<" "<<char(Dung)<<" "<<char(15);
	
	gotoxy(HANG/2+x-7,y-2);	cout<<char(GiuaDuoi)<<"  "<<char(GiuaDuoi)<<" "
								<<char(TraiDuoi)<<char(Ngang)<<" "
								<<char(GiuaDuoi)<<" "<<char(TraiDuoi)<<" "
								<<char(TraiDuoi)<<char(Ngang)<<char(PhaiDuoi);
	
}


void VeDoc(int x,int y,int Dai)
{
	for(int i=0;i<Dai;i++) 
	{
		gotoxy(x,y+i+1); cout<<char(Dung);
	}
}

void VeNgang(int x, int y, int Dai)
{
	gotoxy(x,y);
	for(int i=0; i<Dai; i++) cout<<char(Ngang);
}

void VeKhung(int x, int y, int COT, int HANG)
{
	
	
	gotoxy(x,y);
	cout<<char(TraiTren);
	VeNgang(x+1,y,HANG-1);
	cout<<char(PhaiTren);
	
	gotoxy(x,COT+y);
	cout<<char(TraiDuoi);
	VeNgang(x+1,COT+y,HANG-1);
	cout<<char(PhaiDuoi);
	
	gotoxy(x,y+1);
	VeDoc(x,y,COT-1);
	
	gotoxy(x+HANG,y+1);
	VeDoc(x+HANG,y,COT-1);

//	for(int j=1; j<COT;j++)
//	{
//		gotoxy(x,y+j);cout<<char(Dung);
//	} 
//	
//	for(int j=1; j<COT;j++)
//	{
//		gotoxy(x+HANG,y+j);cout<<char(Dung);
//	} 
	
}



int menu(str ThaoTac[13], int n)
{
	int tt=0, z = 0;
	int *mau = new int [n];	
	ChuMenu(45,5,76);
	VeKhung(45,5,14,76);
	while(1)
	{
		//clrscr();
		textcolor(0);
		gotoxy(55,20); cout<<"Nhan phim LEN, XUONG de chon va ENTER de xac nhan chon!";
		
		textcolor(MAUNEN);
		for(int i=0; i<n;i++) mau[i] = MAUCHU;
		mau[tt] = MAUNEN;
		
		for(int i=0; i<n;i++)
		{
			gotoxy(47,6+i);
			textcolor(mau[i]);
			cout<<setw(2)<<i+1<<") " <<ThaoTac[i]<<endl;
		}
		while(z!=72 && z!=80 && z!=13)
		{
			z = getch();		
		}
		switch(z)
		{
			case 72:{
				tt--;
				if(tt<0) tt= n-1;
				break;
			}
			case 80: {
				tt++; 
				if(tt>n-1) tt=0;
				break;
			}
			case 13: return tt;
		} z=0;
	}
	
	for(int i=0;i<n; i++)
		mau[i] = MAUCHU;
	mau[tt] = MAUNEN;
	delete [] mau;
}

int NhapSo()
{
	int a=0,b,c;
	do
	{
			b=getch();
			if(b>=48 && b<=57)  { c=b-48; a=a*10 +c; cout<<c;}
		
	} while(b!=13);
	return a;
}
bool NhapBool()
{
	int a, b,c; 
	do
	{
		b=getch();
		if(b==48 || b==49) 
		{ 
			a=b-48; cout<<a; 
			while(1)
			{ 
				c=getch(); 
				if(c==13) break;
			}
		}
	} while(b!=48&&b!=49); return a;
}

void XoaTB(int x, int y, int cot, int hang)
{
	textcolor(7);
	for(int i=0;i<3;i++)
	{
		gotoxy(x,y+i); for(int j=0; j<cot+1; j++) cout<<char(219);
	}
	
	textcolor(0);
}

void ThongBao(string a, int DoDaiChu, int x, int y, int hang, int cot)
{
	//textcolor(12);
	VeKhung(x,y,2, hang);
	gotoxy(x+DoDaiChu,y+1);
	cout<<a;	
	textcolor(0);
	//XoaTB(x,y,cot,hang); 
}

void NhayTB(string a, int DoDaiChu, int x, int y, int hang, int cot)
{
	textcolor(12);	VeKhung(x,y,2, hang);	gotoxy(x+DoDaiChu,y+1);	cout<<a;	
	Sleep(200); 
	textcolor(0);	ThongBao(a,DoDaiChu,x,y,hang,cot) ;
}


void ChuanHoa(char a[], int n)
{	
	if(a[0]>90) a[0]-=32;	
	for(int i=1;i<=n;i++)
		if(a[i-1]==32 && a[i]>90) a[i]-=32;
}

void ChuanHoaMa(char a[], int n){
	for(int i=0; i<=n;i++)
		if(a[i]>90) a[i]-=32;
}


void Pwd (char S[], int x, int y) {
	
	Yescursortype();
     int i=-1; gotoxy(x,y);
     while (1) 
     { 
	  i++ ; 
	  S[i] = getch();
	  if(i!=23 && (S[i]>=97 && S[i]<=122) || (S[i]>=48 && S[i]<=57)) cout<<"*";
	  else if(S[i]==8){ 
	  	
	  	if(i>0) { gotoxy(wherex()-1, wherey()); cout<<" ";
	  		i-=2; gotoxy(wherex()-1, wherey());
	  	}
		else {
			i--; 
		}
	  }
	  else if(S[i]==13){i--; break;	  }
	  else if(i==23) i--;
	  else i--;
     }
     S[i+1]='\0';
	Nocursortype();
}

int CheckPwd () {
    int dem =0; char S[30]; int z; 
    for ( dem =1 ; dem <=3 ; dem++)
    { 
		Pwd(S,38,45);
      	if (strcmp(S,PASSWORD) ==0)   return 1;
      	else {
     	 	do{      			
     	 		ThongBao ("Password sai. ENTER: nhap lai!",1, 27,49,32,1)  ; 
      			z=getch();
      		} while(z!=13);
      		XoaTB(27,49,32,1);
      		gotoxy(38,45); textcolor(7); for(int i=0;i<23;i++) cout<<char(219); textcolor(0);
     	 }
    }
    ThongBao ("Ban da nhap qua 3 lan!",1, 30,49,23,1)  ; 
    cout<<endl<<endl;
    exit(0);  
} 




