#pragma once
#include <iostream>
#include "ngaythang.h"
#include "nhat.h"
#include "menu.h"
#include <fstream>
#include "nhapdg.h"
#include "dausach.h"
using namespace std;

 //int STT=0;
long int STTXMT=0;
struct NgayThang
{
	int Ngay = 0;
	int Thang = 0;
	int Nam = 0;
};
struct MuonTra
{
	int MaSach;
	NgayThang NgayMuon;
	NgayThang NgayTra;
	int TrangThai = 0;
};

struct NodeMT
{
	int MaDG;
	MuonTra MT;
	struct NodeMT *next;	
};

typedef struct NodeMT *PTR;
PTR First;

void KhoiTao(PTR &First)
{
	First =NULL;
}

void Insert_First(PTR &First, MuonTra M)
{
	PTR p = new NodeMT;
	p->MT = M;
	p->next = First;
	First = p;
}

int Insert_After(PTR p, MuonTra MT)
{
	PTR t;
	if(p==NULL) return 0;
	else{
		t = new NodeMT;
		t->MT = MT;
		t->next = p->next;
		p->next = t;
	}
}

int Insert_Order(PTR &First, MuonTra MT) 
{
	PTR p, q;
	q=NULL;
	for(p=First; p!=NULL; p=p->next)
		q=p;
	if(q==NULL) Insert_First(First, MT);
	else Insert_After(q, MT);
}
//
//int Delete_First(PTR &First)
//{
//	PTR p;
//	if(First == NULL) return 0;
//	else 
//	{
//		p=First;
//		First = p->next;
//		delete p;
//	}
//}
//
//int Delete_After(PTR p)
//{
//	PTR t, q;
//	if(p==NULL) return 0;
//	else 
//	{
//		q=p->next;
//		p->next = q->next;
//		delete q;
//	}
//}

void Clear_List(PTR &First)
{
	PTR p,t;
	while(First!=NULL)
	{
		p=First;
		First = First->next;
		delete p;
	}
}

PTR Search_infor(PTR &First, int x, int &dem)
{
	PTR p; dem=0;
	p=First;
	for(p=First; p!=NULL ;p=p->next) {
		dem++;
		if(p->MT.MaSach==x && p->MT.TrangThai==0) 	return p;
	}
}
//
//void Selection_Sort(PTR &First)
//{
//	PTR p,t, pmin;
//	SV min;
//	for(p=First;p->next!=NULL;p=p->next)
//	{
//		min=p->sv;
//		pmin=p;
//		for(t=p->next; t!=NULL; t=t->next)
//			if(min.Ma>t->sv.Ma)
//			{
//				min=t->sv;
//				pmin=t;
//			}
//	}
//}

int KTDate(int &n, int &th, int &ng)
{
	do
	{	textcolor(7); gotoxy(119,29); for(int i=0;i<5;i++) cout<<char(219); textcolor(0);
	Yescursortype();
		if(NhapMa(n,119,29,117,5,1,9999)==27) return 27;
		Nocursortype();
		if(ktNam(n)!=1){ gotoxy(118,17); ThongBao("Nam khong hop le! Nhap lai!",1,117,5,29,3); Sleep(500); XoaTB(117,5,29,3); }
	} while(ktNam(n)!=1);
	do
	{	textcolor(7); gotoxy(119,30); for(int i=0;i<5;i++) cout<<char(219); textcolor(0);
	Yescursortype();
		gotoxy(119,30);  if(NhapMa(th,119,30,117,5,0,99)==27) return 27;
		Nocursortype();
		if(ktThang(th,n)!=1){ gotoxy(118,17); ThongBao("Thang sai! Nhap lai!",1,123,5,21,3); Sleep(500); XoaTB(123,5,21,3); }
	} while(ktThang(th,n)!=1);
	do
	{	textcolor(7); gotoxy(119,31); for(int i=0;i<5;i++) cout<<char(219); textcolor(0);
	Yescursortype();
		gotoxy(119,31); if(NhapMa(ng,119,31,117,5,0,99)==27) return 27;
		Nocursortype();
		if(ktNgay(ng, th, n)!=1){ gotoxy(118,17); ThongBao("Ngay sai! Nhap lai!",1,123,5,20,3); Sleep(500); XoaTB(123,5,20,3); }
	} while(ktNgay(ng, th, n)!=1);
}

void XoaNhapMT()
{
	textcolor(7);
	gotoxy(110,26); for(int i=0;i<37;i++) cout<<" ";
	gotoxy(118,29); for(int i=0; i<20;i++) cout<<" ";
	gotoxy(118,30);	for(int i=0; i<20;i++) cout<<" ";
	gotoxy(118,31);	for(int i=0; i<20;i++) cout<<" ";
	gotoxy(113,39); for(int i=0;i<34;i++) cout<<" ";
	textcolor(0);
}

int NhapMuon(MuonTra &A, int &STT, DauSach DS[], int n)
{
	STT++; int kt=0;
	while(kt!=1){		
		textcolor(7);gotoxy(111,26); for(int i=0; i<20;i++) cout<<char(219); textcolor(0);
		ThongBao("MA SACH co 8 chu so",1,120,18,20,3);
		Yescursortype();
		if(NhapMa(A.MaSach,111,26,117,5,10000000,99999999)==27) return 27;	
		Nocursortype();
		XoaTB(120,18,20,3);
		for(int i=0; i<n;i++){
			for(PTRS p=DS[i].ConTroS; p!=NULL;p=p->next){
				if(p->S.MaSach==A.MaSach) {
					if(p->S.TrangThai==1){ kt=2;
						ThongBao("Sach nay da duoc muon!",1,120,5,23,3); Sleep(500); XoaTB(120,5,23,3); continue;
					} 
					else if(p->S.TrangThai==2) {
						kt=2;  ThongBao("Sach da duoc thanh ly!",1,120,5,23,3); Sleep(500); XoaTB(120,5,23,3);
					}
					else {p->S.TrangThai=1; kt=1;}
				}
			}
		}
		if(kt==0){
			ThongBao("Khong co sach nay trong thu vien!",1,115,5,34,3); Sleep(500); XoaTB(115,5,34,3);
		}
	}
	if(KTDate(A.NgayMuon.Nam, A.NgayMuon.Thang, A.NgayMuon.Ngay)==27) return 27;	
}

void XuatM(int MaDG, MuonTra A, int dem, int STT)
{
	gotoxy(4,5+dem*2); cout<<STT;
	gotoxy(10,5+dem*2); cout<<A.MaSach;
	gotoxy(23,5+dem*2); cout<<A.NgayMuon.Ngay<<"/"<<A.NgayMuon.Thang<<"/"<<A.NgayMuon.Nam;
	gotoxy(39,5+dem*2); cout<<A.NgayTra.Ngay<<"/"<<A.NgayTra.Thang<<"/"<<A.NgayTra.Nam;
	gotoxy(56,5+dem*2); if(A.TrangThai==0) cout<<"Dang Muon";
	else if(A.TrangThai==1) cout<<"Da Tra";
	else cout<<"Lam Mat";
	gotoxy(73,5+dem*2); cout<<MaDG;
}

void XuatX(int MaDG, int dem, int STT, MuonTra A)
{
	gotoxy(4,21+dem*2); cout<<STT;
	gotoxy(10,21+dem*2); cout<<A.MaSach;
	gotoxy(23,21+dem*2); cout<<A.NgayMuon.Ngay<<"/"<<A.NgayMuon.Thang<<"/"<<A.NgayMuon.Nam;
	gotoxy(39,21+dem*2); cout<<A.NgayTra.Ngay<<"/"<<A.NgayTra.Thang<<"/"<<A.NgayTra.Nam;
	gotoxy(56,21+dem*2); 
	gotoxy(56,21+dem*2); 
	if(A.TrangThai==0) cout<<"Dang Muon";
	else if(A.TrangThai==1) cout<<"Da Tra";
	else cout<<"Lam Mat";
	gotoxy(73,21+dem*2); cout<<MaDG;
}

void XuatDSMT(PTR First, int Ma)
{
	PTR p;	
	int STT=0, dem=0;
	for(p=First; p!= NULL; p=p->next)	
		if(dem<=24 && p->MT.NgayTra.Nam==0) {
			dem++; STT++;	XuatX(Ma,dem,STT,p->MT);
		}
}

void KhungMuonTra()
{
	gotoxy(30,2); cout<<"BANG MUON TRA";
	VeKhung(3,4,50,80);
	
	VeNgang(3,6,80);
	gotoxy(3,6); cout<<char(GiuaTrai);
	gotoxy(83,6); cout<<char(GiuaPhai);	
	
	VeDoc(9,4,50);
	gotoxy(9,4); cout<<char(GiuaTren);
	gotoxy(9,54); cout<<char(GiuaDuoi);
	gotoxy(9,6); cout<<char(GiuaGiua);
	gotoxy(5,5); cout<<"STT";
	
	VeDoc(22,4, 50);
	gotoxy(22,4); cout<<char(GiuaTren);
	gotoxy(22,54); cout<<char(GiuaDuoi);
	gotoxy(22,6); cout<<char(GiuaGiua);
	gotoxy(12,5); cout<<"MA SACH";
	VeDoc(38,4,50);
	gotoxy(38,4); cout<<char(GiuaTren);
	gotoxy(38,54); cout<<char(GiuaDuoi);
	gotoxy(38,6); cout<<char(GiuaGiua);
	gotoxy(26,5); cout<<"NGAY MUON";
	
	VeDoc(54,4, 50);
	gotoxy(54,4); cout<<char(GiuaTren);
	gotoxy(54,54); cout<<char(GiuaDuoi);
	gotoxy(54,6); cout<<char(GiuaGiua);
	gotoxy(42,5); cout<<"NGAY TRA";	
	
	gotoxy(58,5); cout<<"TRANG THAI";
	VeDoc(71,4,50);	
	gotoxy(71,4); cout<<char(GiuaTren);
	gotoxy(71,54); cout<<char(GiuaDuoi);
	gotoxy(71,6); cout<<char(GiuaGiua);
	gotoxy(74,5); cout<<"MA DG";
	for(int i=1; i<24;i++){
		gotoxy(4,6+i*2); for(int j=1;j<80;j++){
			if(j==6 || j==19 || j==35 || j==51 || j==68) gotoxy(j+4,6+i*2);
			else cout<<char(196);
		}
	}
}

void KhungNhapMT()
{
	VeKhung(100,23,10,57);
	//VeKhung(100,25,2,55);	
	gotoxy(102,24); cout<<"MA DOC GIA: ";
	gotoxy(102,26); cout<<"MA SACH: ";
	//VeKhung(100,28,4,55);
	gotoxy(102,29); cout<<"NGAY MUON: NAM: ";
	gotoxy(102,30); cout<<"           THANG: ";
	gotoxy(102,31); cout<<"           NGAY: ";
	
	
	//VeKhung(100,38,2,55);
	//gotoxy(102,39); cout<<"TRANG THAI: ";
}


void KhungTra(){
	VeKhung(100,30,5,55);
	gotoxy(102,34); cout<<"MAT SACH(1: Mat Sach, 2: Tra Sach): ";
}

void NhapTra(MuonTra &A){
	NgayHienTai(A.NgayTra.Nam, A.NgayTra.Thang, A.NgayTra.Ngay);
}

void XoaXuatMT(){
	textcolor(7);
	for(int i=0; i<47;i++){
		if((i+1)%2==0) ;
		else {
			gotoxy(4,7+i); for(int j=0;j<5;j++)	 cout<<" "; 
		gotoxy(10,7+i); for(int j=0; j< 12 ;j++) cout<<" "; 
		gotoxy(23,7+i); for(int j=0; j< 15 ;j++) cout<<" "; 
		gotoxy(39,7+i); for(int j=0; j< 15 ;j++) cout<<" ";
		gotoxy(55,7+i); for(int j=0; j< 15 ;j++) cout<<" "; 
		gotoxy(72,7+i);  for(int j=0; j< 11 ;j++) cout<<" ";
		}
	}
	textcolor(0);
}

void KhungMuonTraDG()
{
	gotoxy( 30,18); cout<<"CAC SACH MA DOC GIA DANG MUON";
	VeKhung(3,20,8,80);
	
	VeNgang(3,22,80);
	gotoxy(3,22); cout<<char(GiuaTrai);
	gotoxy(83,22); cout<<char(GiuaPhai);	
	
	VeDoc(9,20,8);
	gotoxy(9,20); cout<<char(GiuaTren);
	gotoxy(9,28); cout<<char(GiuaDuoi);
	gotoxy(9,22); cout<<char(GiuaGiua);
	gotoxy(5,21); cout<<"STT";
	
	VeDoc(22,20, 8);
	gotoxy(22,20); cout<<char(GiuaTren);
	gotoxy(22,28); cout<<char(GiuaDuoi);
	gotoxy(22,22); cout<<char(GiuaGiua);
	gotoxy(12,21); cout<<"MA SACH";
	VeDoc(38,20,8);
	gotoxy(38,20); cout<<char(GiuaTren);
	gotoxy(38,28); cout<<char(GiuaDuoi);
	gotoxy(38,22); cout<<char(GiuaGiua);
	gotoxy(26,21); cout<<"NGAY MUON";
	
	VeDoc(54,20, 8);
	gotoxy(54,20); cout<<char(GiuaTren);
	gotoxy(54,28); cout<<char(GiuaDuoi);
	gotoxy(54,22); cout<<char(GiuaGiua);
	gotoxy(42,21); cout<<"NGAY TRA";	
	
	gotoxy(58,21); cout<<"TRANG THAI";
	VeDoc(71,20,8);	
	gotoxy(71,20); cout<<char(GiuaTren);
	gotoxy(71,28); cout<<char(GiuaDuoi);
	gotoxy(71,22); cout<<char(GiuaGiua);
	gotoxy(74,21); cout<<"MA DG";
	for(int i=1; i<3;i++){
		gotoxy(4,22+i*2); for(int j=1;j<80;j++){
			if(j==6 || j==19 || j==35 || j==51 || j==68) gotoxy(j+4,22+i*2);
			else cout<<char(196);
		}
	}
}


void XoaXuatMTDG(){
	//textcolor(0);
	for(int i=0; i<6;i++){
		if((i+1)%2==0) ;
		else {
			gotoxy(4,23+i); for(int j=0;j<5;j++)	 cout<<" "; 
		gotoxy(10,23+i); for(int j=0; j< 12 ;j++) cout<<" "; 
		gotoxy(23,23+i); for(int j=0; j< 15 ;j++) cout<<" "; 
		gotoxy(39,23+i); for(int j=0; j< 15 ;j++) cout<<" ";
		gotoxy(55,23+i); for(int j=0; j< 15 ;j++) cout<<" "; 
		gotoxy(72,23+i);  for(int j=0; j< 11 ;j++) cout<<" ";
		}
	}
	//textcolor(0);
}








