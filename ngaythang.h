#pragma once
#include <iostream>
#include <ctime>
#include <fstream>
#include "nhat.h"
#include "menu.h"
using namespace std;


int namnt, thangnt, ngaynt;

int NamNhuan(int n)
{
	if(n%400==0 || (n%100!=0 && n%4==0)) return 1;
	return 0;
}

int NgayHienTai( int &nam, int &thang, int &ngay )
{
   	// tra ve date/time hien tai dua tren system hien tai
 	time_t baygio = time(0);
	tm *ltm = localtime(&baygio);
	nam = 1900 + ltm->tm_year;
	thang = 1 + ltm->tm_mon;
	ngay = ltm->tm_mday;
}

int ktNam(int n)
{
	NgayHienTai(namnt, thangnt, ngaynt);
	if(n>namnt || n<namnt-1) return 0;
	return 1;
}

int ktThang(int th, int n)
{
	NgayHienTai(namnt, thangnt, ngaynt);
	if(th<1 || th>12 ||(n<namnt && th<thangnt) || (n==namnt&&th>thangnt)) return 0;
	return 1;
}

int ktNgay(int ng, int th, int n)
{
	NgayHienTai(namnt, thangnt, ngaynt);
	if((n<namnt && th == thangnt && ng<=ngaynt) || (n==namnt && th == thangnt && ng>ngaynt)) return 0;
	if(ng<1 || ng>31) return 0;
	if(th==2)
	{
		if(NamNhuan(n)==1 && ng >29) return 0;
		else if(NamNhuan(n)==0 && ng>28) return 0;
	}
	else if(th==2 || th==4 || th==6 || th==9 || th==11)
		if(ng>30) return 0;
	return 1;
}

int DemSoNgay(int ngay1, int thang1, int nam1, int ngay, int thang, int nam)
{
	int demngay=0, demthang=0, demnam=0;
	
	for (int i=nam1;i<nam;i++)
        if( NamNhuan(i) ==1) demnam+=366; 
        else demnam+=365;
 
    /* Tinh khoang cach so ngay giua 2 thang */
    int a[]={31,28,31,30,31,30,31,31,30,31,30,31};
    if ( NamNhuan(nam)) a[1]=29;          
    if (thang1>thang)
    {
        for (int i=thang;i<thang1;i++)
            demthang-=a[i-1];
    }
    else
    {
        for (int i=thang1;i<thang;i++)
            demthang+=a[i-1];
    }
    demngay=ngay-ngay1;
	demngay = demngay +demthang+demnam;  
    
    return demngay;    
}

void docctdl(){
	string s;
	ifstream d("ctdl.txt", ios::in);
	while(!d.eof()){
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		textcolor(17);
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		textcolor(11);
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		textcolor(17);
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		textcolor(11);
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		textcolor(17);
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		textcolor(11);
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		textcolor(17);
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
		getline(d,s);	cout<<s;	cout<<endl;
	}
	d.close();
}
void TrangDau(){
	
	docctdl(); 
	textcolor(12);
	gotoxy(25,23); cout<<"HOC VIEN CONG NGHE BUU CHINH VIEN THONG";
	gotoxy(36,24); cout<<"CO SO TAI TP.HCM";
	gotoxy(26,27); cout<<"DE TAI CAU TRUC DU LIEU VA GIAI THUAT";
	gotoxy(30,29); cout<<"TEN DE TAI: QUAN LI THU VIEN.";
	gotoxy(18,31); cout<<"GV HUONG DAN: LUU NGUYEN KY THU";
	gotoxy(18,33); cout<<"TEN SV: NGUYEN ANH NHAT		MA SV: N15DCCN037";	
	gotoxy(18,35); cout<<"LOP: D15CQCN01-N";
	SetBGColor(1);
	textcolor(11);
	VeKhung(26,44,2,35);
	VeKhung(0,0,57,166); gotoxy(27,45); cout<<"PASSWORD: ";
	
	SetBGColor(7); 	textcolor(0);
	CheckPwd();
	
	SetBGColor(7); 	textcolor(7); VeKhung(26,42,4,35);
	VeNgang(27,44,34);   gotoxy(27,43); cout<<"ID:       ";
	VeKhung(0,0,57,166); gotoxy(27,45); cout<<"PASSWORD: ";
	clrscr(); 
}

