#pragma one
#include <iostream>
#include "ngaythang.h"
#include "muontra.h"
#include "docgia.h"

using namespace std;

struct QuaHan{
	DocGia DG;
	MuonTra MT;
	int songay;
};
struct NodeQH{
	QuaHan QH;
	struct NodeQH *left;
	struct NodeQH *right;
};
typedef struct NodeQH *PTRqh;
PTRqh CayQH;

void InsertNodeQH(PTRqh &p,QuaHan Q)
{
	if(p==NULL)
	{
		p=new NodeQH;
		p->QH = Q;
		p->left = p->right = NULL;
	}
	else if(Q.songay<p->QH.songay) InsertNodeQH(p->left,Q);
	else if(Q.songay>p->QH.songay) InsertNodeQH(p->right,Q);
}
void KhoiTaoQH(PTRqh &cay)
{
	cay =NULL;
}


int N1, T1, NG1, QH, mQH=0, ktqh;

void AddTreeQH(PTRDG p, PTRqh &cay)
{
	QuaHan a;
	if(p!=NULL)
	{ 
		AddTreeQH(p->left,cay);
		for(PTR q = p->DG.ConTroDG; q!=NULL; q=q->next)
		{
			NgayHienTai(N1, T1, NG1);
			QH = 0;
			QH = DemSoNgay(q->MT.NgayMuon.Ngay, q->MT.NgayMuon.Thang, q->MT.NgayMuon.Nam, NG1, T1, N1);
			if(QH> 15 && q->MT.NgayTra.Nam==0){
				p->DG.TTThe=0;
				a.DG = p->DG; a.MT = q->MT; a.songay = QH;
				InsertNodeQH(cay,a);
			}
		}
		AddTreeQH(p->right,cay);
	}
}

PTRDG InorderQH(PTRDG &p, PTRqh &cay)
{
	if(p!=NULL){
		InorderQH(p->left,cay);	
		AddTreeQH(p,cay);
		InorderQH(p->right,cay);
	}
	
}


void InDGQH(QuaHan A, int &STT, int &dem)
{	
	++STT;
	gotoxy(2,3+dem*2); cout<<STT;
	gotoxy(9,3+dem*2); cout<<A.DG.MaDG;
	gotoxy(20,3+dem*2); cout<<A.DG.Ho<<" ";cout	<<A.DG.Ten;
	gotoxy(54,3+dem*2); cout<<A.MT.MaSach;
	gotoxy(69,3+dem*2); cout<<A.songay;
	
}

void DemQH(PTRqh p, int &m){
	if(p!=NULL){
		DemQH(p->right,m);
		m++;
		DemQH(p->left,m);
	}
}

void InorderXQH(PTRqh p, int &i, int &dem, int &STT, int &dau, int &cuoi)
{
	if(p!=NULL)	{
		InorderXQH(p->right,i, dem,STT,dau, cuoi);
		i++;
		if(i>=dau && i<=cuoi){
			InDGQH(p->QH,STT,dem);  dem++; 
			if(i==26)return;
		} 
		InorderXQH(p->left,i,dem,STT, dau, cuoi);
	}
}

void KhungQH()
{
	gotoxy(32,1); cout<<"BANG THONG TIN QUA HAN";
	int a1=1, b4 = 2, Cot = 54, Hang = 78;
	VeKhung(a1,b4,Cot,Hang);
	
	VeDoc(a1+6,b4,Cot-1);
	VeNgang(a1+1,b4+2,Hang-1);	
	gotoxy(a1,b4+2); cout<<char(GiuaTrai);
	gotoxy(a1+6,b4); cout<<char(GiuaTren);
	gotoxy(a1+6,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+6,Cot+2); cout<<char(GiuaDuoi);
	gotoxy(a1+Hang,b4+2); cout<<char(GiuaPhai);
	gotoxy(a1+1,b4+1); cout<<"STT";
	
	gotoxy(a1+7,b4+1); cout<<"MA DOC GIA";
	VeDoc(a1+17,b4,Cot-1);
	gotoxy(a1+17,b4); cout<<char(GiuaTren);
	gotoxy(a1+17,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+17,Cot+2); cout<<char(GiuaDuoi);
	
	gotoxy(a1+31,b4+1); cout<<"HO VA TEN";
	
	gotoxy(a1+56,b4+1); cout<<"MA SACH";
	VeDoc(a1+51,b4,Cot-1);
	gotoxy(a1+51,b4); cout<<char(GiuaTren);
	gotoxy(a1+51,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+51,Cot+2); cout<<char(GiuaDuoi);	
	
	gotoxy(a1+66,b4+1); cout<<"SO NGAY MUON";
	VeDoc(a1+65,b4,Cot-1);
	gotoxy(a1+65,b4); cout<<char(GiuaTren);
	gotoxy(a1+65,b4+2); cout<<char(GiuaGiua);
	gotoxy(a1+65,Cot+2); cout<<char(GiuaDuoi);
	for(int i=6;i<55;i+=2){
		gotoxy(a1+1,i); for(int j=0;j<77;j++) {
			if(j==5 ||j==16 || j==50 || j==64) gotoxy(2+j+1,i);
			else cout<<char(196);
		}
	}	
}


void XoaXuatQH(){
	for(int i=0; i<51;i++){
		if((i+1)%2==0) ;
		else {
			gotoxy(2,5+i); for(int j=0;j<5;j++)	 cout<<" "; 
		gotoxy(8,5+i); for(int j=0; j< 10 ;j++) cout<<" "; 
		gotoxy(19,5+i); for(int j=0; j< 33 ;j++) cout<<" "; 
		gotoxy(53,5+i); for(int j=0; j< 13 ;j++) cout<<" "; 
		gotoxy(67,5+i); for(int j=0; j< 12 ;j++) cout<<" "; 
		}
	}
}


void XuatDGQuaHan(PTRDG &root, PTRqh &cay)
{
	int z;	
	int dem=1, STT=0, i=0,  dau =1, cuoi = 26, m=0; 
	InorderQH(root,cay); DemQH(cay,m);
	ThongBao("UP: TRANG TRUOC", 1, 100,36,16,3); ThongBao("DOWN: TRANG SAU", 1, 120,36,16,3);
	ThongBao("ESC: Ve MENU", 1 , 112,41,13,3);
	if(cay == NULL)	 { cout<<"Khong co thong tin de xuat!\n\n"; getch();}
	else{				
		KhungQH();
		InorderXQH(cay,i, dem,STT,dau, cuoi); 
		while(1){	 					
			z  = getch();	
			if(z==72) NhayTB("UP: TRANG TRUOC", 1, 100,36,16,3);
			if(z==80) NhayTB("DOWN: TRANG SAU", 1, 120,36,16,3);
			if(z==27){NhayTB("ESC: Ve MENU", 1 , 112,41,13,3);	return;}			
			else if(z==72 && STT>26){
				if(STT%26==0) STT-=52;
				else {
					dau=STT%26; STT = STT - 26 - dau;
				}
				dem=1; dau = STT+1; cuoi =STT + 26; 	i=0;		
				XoaXuatQH();		
				InorderXQH(cay,i, dem,STT,dau, cuoi);  	
			}
			else if(z==80 && STT!=m){
				if(m<STT+26) cuoi = m;
				else  cuoi =STT + 26;
				dem=1; dau = STT+1;	i=0;			
				XoaXuatQH();	
				InorderXQH(cay,i, dem,STT,dau, cuoi);
			}
		}
	}	
}




